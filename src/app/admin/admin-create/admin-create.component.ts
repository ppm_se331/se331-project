import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import Activity from 'src/app/entity/activity';
import { ActivityService } from 'src/app/service/activity-service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material';
import { AddconfirmationDialogComponent } from 'src/app/shared/addconfirmation-dialog/addconfirmation-dialog.component';
@Component({
  selector: 'app-admin-create',
  templateUrl: './admin-create.component.html',
  styleUrls: ['./admin-create.component.css']
})
export class AdminCreateComponent  {

  activitys: Activity[];
  form = this.fb.group({
    activityId: [''],
    actName: [''],
    location: [''],
    date: [''],
    time: [''],
    preriodOfRegis: [''],
    host: [''],
    description: ['']
  })

  constructor(private fb: FormBuilder,private activityService: ActivityService,private router: Router,public dialog: MatDialog) {}

  
  submit(): void{
    // const dialogRef = this.dialog.open(AddconfirmationDialogComponent, {
    //   width: '350px',
    //   data: "Are you sure to confirm the enrollment to this activity?"
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if(result) {
    //     console.log('Yes clicked');
    //   }
    // });
    alert('Activity was created');
  }
}
