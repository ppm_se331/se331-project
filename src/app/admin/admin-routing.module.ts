import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDashboardComponent } from '../admin/admin-dashboard/admin-dashboard.component';
import { AdminCreateComponent } from '../admin/admin-create/admin-create.component';
import { AdminStudentsTableComponent } from '../admin/admin-students-table/admin-students-table.component';


const AdminRoutes: Routes = [
 
    { path: 'admin', component: AdminDashboardComponent },
    { path: 'admin-create', component: AdminCreateComponent },
    { path: 'admin-student', component: AdminStudentsTableComponent}
];


@NgModule({
    imports: [
        RouterModule.forRoot(AdminRoutes)
    ],
    exports: [RouterModule]
})

export class AdminRoutingModule { }
export const routingComponent = [AdminDashboardComponent,AdminStudentsTableComponent, AdminCreateComponent]
