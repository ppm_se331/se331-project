import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import Student from 'src/app/entity/student';
import { BehaviorSubject } from 'rxjs';
import {MatDialog} from '@angular/material';
import { StudentService } from 'src/app/service/student-service';
import { StudentTableDataSource } from 'src/app/students/student-table/student-table-datasource';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
@Component({
  selector: 'app-admin-students-table',
  templateUrl: './admin-students-table.component.html',
  styleUrls: ['./admin-students-table.component.css']
})
export class AdminStudentsTableComponent implements OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  dataSource: StudentTableDataSource;
  

  ngAfterViewInit(){
  }

  
  displayedColumns = ['profileStudent','numberStudent','nameStudent', 'surnameStudent', 'dateStudent','emailStudent', 'passwordStudent','enrollButton'];
  student: Student[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private studentService: StudentService,public dialog: MatDialog) { }
  ngOnInit() {
    this.studentService.getStudents()
      .subscribe(student => {
        this.dataSource = new StudentTableDataSource();
        this.dataSource.data = student;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.student = this.student;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }
  Approvebutton(id: any): void {
    console.log(id);
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Are you sure to Approve to this student?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log('Yes clicked');
        // DO SOMETHING
        if(id == '002'){
          console.log(document.getElementById("Approvebtn002"));
          document.getElementById("Approvebtn002").innerHTML = "Approved!!!";
          document.getElementById("rejectBtn002").innerHTML = "";
        }
        if(id == '001'){
          console.log(document.getElementById("Approvebtn001"));
          document.getElementById("Approvebtn001").innerHTML = "Approved!!!";
          document.getElementById("rejectBtn001").innerHTML = "";
        }
        if(id == '003'){
          console.log(document.getElementById("Approvebtn003"));
          document.getElementById("Approvebtn003").innerHTML = "Approved!!!";
          document.getElementById("rejectBtn003").innerHTML = "";
        }
        
      }

    });
  }

  rejectbutton(id: any): void {
    console.log(id);
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Are you sure to Reject to this student?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log('Yes clicked');
        // DO SOMETHING
        if(id == '002'){
          console.log(document.getElementById("rejectBtn002"));
          document.getElementById("rejectBtn002").innerHTML = "rejected!!!";
          document.getElementById("Approvebtn002").innerHTML = "";
        }
        if(id == '001'){
          console.log(document.getElementById("rejectBtn001"));
          document.getElementById("rejectBtn001").innerHTML = "rejected!!!";
          document.getElementById("Approvebtn001").innerHTML = "";
        }
        if(id == '003'){
          console.log(document.getElementById("rejectBtn003"));
          document.getElementById("rejectBtn003").innerHTML = "rejected!!!";
          document.getElementById("Approvebtn003").innerHTML = "";
        }
        
      }

    });
  }

  
  applyFilter(filterValue: any) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

}
