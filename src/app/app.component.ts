import { Component } from '@angular/core';
import { AmazingTimePickerService } from '../../node_modules/amazing-time-picker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name = 'SE331';

  constructor( private atp: AmazingTimePickerService, // this line you need
  ) { }

  open() {
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      console.log(time);
    });
  }

}