import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentService } from './service/student-service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentsFileImplService } from './service/students-file-impl.service';
import { StudentsComponent } from './students/list/students.component';
import { StudentsAddComponent } from './students/add/students.add.component';
import { StudentsViewComponent } from './students/view/students.view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent, RegisterFormComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSortModule } from '@angular/material/sort';
import { AppRoutingModule } from './app-routing.module';
import { StudentRoutingModule } from './students/student-routing.module';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { StudentTableComponent } from './students/student-table/student-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule, MatDialogModule, MatNativeDateModule } from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { routingComponent } from './students/student-routing.module'
import { ActivityService } from './service/activity-service';
import { StudentEnrollmentComponent } from './students/student-enrollment/student-enrollment.component';
import { ActivitypFileImplService } from './service/activityp-file-impl.service';
import { MatGridListModule } from '@angular/material/grid-list';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component'
import { AdminCreateComponent } from './admin/admin-create/admin-create.component';
import { AdminStudentsTableComponent } from './admin/admin-students-table/admin-students-table.component';
import { ActivityRestImplService } from './service/activity-rest-impl';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmationDialogComponent } from './shared/confirmation-dialog/confirmation-dialog.component';
import { AddconfirmationDialogComponent } from './shared/addconfirmation-dialog/addconfirmation-dialog.component';
import { RegisterComponent } from './students/register/register.component';
import { MatSelectModule } from '@angular/material/select'
import { MatDatepickerModule } from '@angular/material/datepicker'
import { MatSnackBarModule } from '@angular/material/snack-bar'
import { OnlynumberDirective } from './service/numberOnly';
import { AmazingTimePickerModule } from 'amazing-time-picker'
import { TeacherListComponent } from './teachers/teacher-list/teacher-list.component';
import { TeacherDashboardComponent } from './teachers/teacher-dashboard/teacher-dashboard.component';
import { MaterialFileUploadComponent } from './material-file-upload/material-file-upload.component';
import { MatProgressBarModule } from '@angular/material/progress-bar'

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentsAddComponent,
    StudentsViewComponent,
    MyNavComponent,
    FileNotFoundComponent,
    StudentTableComponent,
    routingComponent,
    StudentEnrollmentComponent,
    AdminDashboardComponent,
    AppComponent,
    AdminCreateComponent,
    AdminStudentsTableComponent,
    ConfirmationDialogComponent,
    AddconfirmationDialogComponent,
    TeacherListComponent,
    TeacherDashboardComponent,
    MaterialFileUploadComponent,
    OnlynumberDirective,
    RegisterComponent,
    RegisterFormComponent
  ],
  entryComponents: [
    ConfirmationDialogComponent,
    AddconfirmationDialogComponent,
    RegisterFormComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSortModule,
    StudentRoutingModule,
    AppRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatGridListModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    AmazingTimePickerModule,
    MatProgressBarModule,
  ],
  providers: [
    { provide: StudentService, useClass: StudentsFileImplService },
    { provide: ActivityService, useClass: ActivitypFileImplService },
    { provide: ActivityService, useClass: ActivityRestImplService }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
