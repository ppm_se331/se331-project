import { Time } from '@angular/common'

export default class Student {
    Profile: string;
    StudentName: string;
    activityId: number;
    actName: string;
    location: string;
    Date: string;
    time: string;
    preriodOfRegis: string;
    host: string;
    description: string;
  }
  