export default class Student {
  id: number;
  studentId: string;
  name: string;
  surname: string;
  gpa: number;
  image: string;
  featured: boolean;
  penAmount: number;
  profileStudent: string;
  numberStudent: number;
  nameStudent: string;
  surnameStudent: string;
  dateStudent: string;
  emailStudent: string;
  passwordStudent: string;
  Profile: string;
  StudentName: string;
  activityId: number;
  actName: string;
  location: string;
  Date: string;
  time: string;
  preriodOfRegis: string;
  host: string;
  description: string;
}
