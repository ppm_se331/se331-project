export default class teacherDialog {
    actId: number;
    actName: string;
    location: string;
    date: string;
    timeStart: string;
    timeTo: string
    actTime: string;
    host: string;
  }
  