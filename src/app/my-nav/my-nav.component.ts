import { Component, Inject } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import Student from '../entity/student';
import { StudentService } from '../service/student-service';
import { MatDialog, MatSnackBar, MatSnackBarConfig } from '../../../node_modules/@angular/material';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})

export class MyNavComponent {

  stdid = ''

  // ---------------- Change Page ---------------- //
  home = true
  student = false
  teacher = false
  admin = false

  callHomePage() {
    this.home = true
    this.student = false
    this.teacher = false
    this.admin = false
  }

  callStudentPage() {
    this.home = false
    this.student = true
    this.teacher = false
    this.admin = false
  }

  callTeacherPage() {
    this.home = false
    this.student = false
    this.teacher = true
    this.admin = false
  }

  callAdminPage() {
    this.home = false
    this.student = false
    this.teacher = false
    this.admin = true
  }

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  students$: Observable<Student[]> = this.studentService.getStudents();

  constructor(private breakpointObserver: BreakpointObserver, private studentService: StudentService,
    public dialog: MatDialog) { }

  openSimpleDialog() {
    const myDialog = this.dialog.open(RegisterFormComponent, {
      width: '800px'
    })
  }
}


@Component({
  selector: 'app-register-form',
  templateUrl: 'register-form/register-form.component.html',
  styleUrls: ['register-form/register-form.component.css']
})
export class RegisterFormComponent {

  constructor(private snackbar: MatSnackBar) { }

  openSnackBar() {
    this.snackbar.open('Registeration Completed', '', {
      duration: 3000
    });
  }

}
