import { Observable } from '../../../node_modules/rxjs';
import Activity from '../entity/activity';
import { ActivityService } from './activity-service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

export class ActivityRestImplService extends ActivityService {
    constructor(private http: HttpClient){
        super();
    }
    getActivitys(): Observable<Activity[]> {
        return this.http.get<Activity[]>(environment.studentApi);
    }
    getActivity(id: number): Observable<Activity> {
        return this.http.get<Activity>(environment.studentApi + '/' + id);
    }
    saveActivity(activity: Activity): Observable<Activity> {
        return this.http.post<Activity>(environment.studentApi, activity);
    }
    
}
