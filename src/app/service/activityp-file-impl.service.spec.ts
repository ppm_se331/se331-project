import { TestBed } from '@angular/core/testing';

import { ActivitypFileImplService } from './activityp-file-impl.service';

describe('ActivitypFileImplService', () => {
  beforeEach(() =>  TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActivitypFileImplService = 
    TestBed.get(ActivitypFileImplService);
    expect(service).toBeTruthy();
  });
});
