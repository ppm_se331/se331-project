import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivityService } from './activity-service';
import { Observable } from 'rxjs';
import Activity from '../entity/activity'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ActivitypFileImplService extends ActivityService {
  saveActivity(activity: Activity): Observable<Activity> {
    throw new Error("Method not implemented.");
  }
  constructor(private http: HttpClient) {
    super();
  }
  getActivity(id: number): Observable<Activity> {
    return this.http.get<Activity[]>('assets/activity.json')
      .pipe(map(Activitys => {
        const output: Activity 
        = (Activitys as Activity[]).find
        (Activity => Activity.activityId === +id);
        return output;
      }));

  }

  getActivitys(): Observable<Activity[]>{
    return this.http.get<Activity[]>('assets/activity.json');

  }
  
}
