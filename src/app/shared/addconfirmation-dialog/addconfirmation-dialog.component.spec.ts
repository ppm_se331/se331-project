import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddconfirmationDialogComponent } from './addconfirmation-dialog.component';

describe('AddconfirmationDialogComponent', () => {
  let component: AddconfirmationDialogComponent;
  let fixture: ComponentFixture<AddconfirmationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddconfirmationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddconfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
