import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder } from '../../../../node_modules/@angular/forms';
import teacherDialog from '../../entity/teacherDialog';

@Component({
  selector: 'app-addconfirmation-dialog',
  templateUrl: './addconfirmation-dialog.component.html',
  styleUrls: ['./addconfirmation-dialog.component.css']
})
export class AddconfirmationDialogComponent  {

  teacherDialogs: teacherDialog[];
  form = this.fb.group({
    actId: [''],
    actName: [''],
    location: [''],
    date: [''],
    timeStart: [''],
    timeTo: [''],
    actTime: [''],
    host: ['']
  })
  
  constructor(
    public dialogRef: MatDialogRef<AddconfirmationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public message: string,
    private fb: FormBuilder) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onFileComplete(data: any) {
    console.log(data); // We just print out data bubbled up from event emitter.
}
}


