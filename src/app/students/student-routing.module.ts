import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentsComponent } from './list/students.component';
import { StudentsViewComponent } from './view/students.view.component';
import { StudentsAddComponent } from './add/students.add.component';
import { StudentDashboardComponent } from './student-dashboard/student-dashboard.component';
import { StudentEnrollmentComponent } from './student-enrollment/student-enrollment.component';
import { TeacherDashboardComponent } from '../teachers/teacher-dashboard/teacher-dashboard.component';
import { AdminDashboardComponent } from '../admin/admin-dashboard/admin-dashboard.component';
import { AdminCreateComponent } from '../admin/admin-create/admin-create.component';
import { AdminStudentsTableComponent } from '../admin/admin-students-table/admin-students-table.component';
import { TeacherListComponent } from '../teachers/teacher-list/teacher-list.component';

const StudentRoutes: Routes = [
    { path: 'student-update', component: StudentDashboardComponent },
    { path: 'student-enrollment', component: StudentEnrollmentComponent },
    { path: 'teacher', component: TeacherDashboardComponent },
    { path: 'teacher-list', component: TeacherListComponent},
    { path: 'admin', component: AdminDashboardComponent },
    { path: 'admin-create', component: AdminCreateComponent },
    { path: 'pending-list', component: AdminStudentsTableComponent}

];


@NgModule({
    imports: [
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [RouterModule]
})

export class StudentRoutingModule { }
export const routingComponent = [StudentDashboardComponent, StudentEnrollmentComponent,AdminDashboardComponent,AdminCreateComponent,AdminStudentsTableComponent,
    TeacherDashboardComponent,TeacherListComponent]
