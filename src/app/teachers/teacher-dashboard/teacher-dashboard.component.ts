import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import Student from 'src/app/entity/student';
import { BehaviorSubject } from 'rxjs';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material';
import { StudentTableDataSource } from 'src/app/students/student-table/student-table-datasource';
import { StudentService } from 'src/app/service/student-service';
@Component({
  selector: 'app-teacher-dashboard',
  templateUrl: './teacher-dashboard.component.html',
  styleUrls: ['./teacher-dashboard.component.css']
})
export class TeacherDashboardComponent implements OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  
  dataSource: StudentTableDataSource;


  ngAfterViewInit(){
  }
  displayedColumns = ['studentid','studentname','studentsurname', 'actName', 'location', 'Date', 'time', 'host', 'description','ImageOfActivity','enrollButton'];
 students: Student[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(public dialog: MatDialog,private studentService: StudentService) { }
  ngOnInit() {
   
      this.studentService.getStudents().subscribe(students => {
        this.dataSource = new StudentTableDataSource();
        this.dataSource.data = students;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.students = students;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }


  Approvebutton(id: any): void {
    console.log(id);
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Are you sure to Approve to this student?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log('Yes clicked');
        // DO SOMETHING
        if(id == '002'){
          console.log(document.getElementById("Approvebtn002"));
          document.getElementById("Approvebtn002").innerHTML = "Approved!!!";
          document.getElementById("rejectBtn002").innerHTML = "";
        }
        if(id == '001'){
          console.log(document.getElementById("Approvebtn001"));
          document.getElementById("Approvebtn001").innerHTML = "Approved!!!";
          document.getElementById("rejectBtn001").innerHTML = "";
        }
        if(id == '003'){
          console.log(document.getElementById("Approvebtn003"));
          document.getElementById("Approvebtn003").innerHTML = "Approved!!!";
          document.getElementById("rejectBtn003").innerHTML = "";
        }
        
      }

    });
  }

  rejectbutton(id: any): void {
    console.log(id);
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Are you sure to Reject to this student?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log('Yes clicked');
        // DO SOMETHING
        if(id == '002'){
          console.log(document.getElementById("rejectBtn002"));
          document.getElementById("rejectBtn002").innerHTML = "rejected!!!";
          document.getElementById("Approvebtn002").innerHTML = "";
        }
        if(id == '001'){
          console.log(document.getElementById("rejectBtn001"));
          document.getElementById("rejectBtn001").innerHTML = "rejected!!!";
          document.getElementById("Approvebtn001").innerHTML = "";
        }
        if(id == '003'){
          console.log(document.getElementById("rejectBtn003"));
          document.getElementById("rejectBtn003").innerHTML = "rejected!!!";
          document.getElementById("Approvebtn003").innerHTML = "";
        }
        
      }

    });
  }
  
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

}

