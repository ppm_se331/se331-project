import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ActivityTableDataSource } from 'src/app/students/student-enrollment/student-enrollment-datasource';
import Activity from 'src/app/entity/activity';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity-service';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material';
import { AddconfirmationDialogComponent } from 'src/app/shared/addconfirmation-dialog/addconfirmation-dialog.component';

@Component({
  selector: 'app-teacher-list',
  templateUrl: './teacher-list.component.html',
  styleUrls: ['./teacher-list.component.css']
})
export class TeacherListComponent implements OnInit {
  
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;

  ngAfterViewInit(){
  }
  displayedColumns = ['ImageOfActivity','activityId', 'actName', 'location', 'Date', 'time', 'preriodOfRegis', 'host', 'description','enrollButton'];
  activities: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private activityService: ActivityService,public dialog: MatDialog) { }
  ngOnInit() {
    this.activityService.getActivitys()
      .subscribe(activities => {
        this.dataSource = new ActivityTableDataSource();
        this.dataSource.data = activities;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.activities = activities;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }

  Upate(): void {
    const dialogRef = this.dialog.open(AddconfirmationDialogComponent, {
      width: '800px'
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('Detaill of Student');
    });
  }
  
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

}
